from django.contrib import admin
from projects.models import Project


class ProjectAdmin(admin.ModelAdmin):

    admin.site.register(Project)
