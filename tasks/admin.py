from django.contrib import admin
from tasks.models import Task


class TaskAdmin(admin.ModelAdmin):

    admin.site.register(Task)
