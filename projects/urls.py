from django.urls import path
from projects.views import project_list, show_project
from projects.views import create_project

urlpatterns = [
    path("<int:id>/", show_project, name="show_project"),
    path("", project_list, name="project_list"),
    path("create/", create_project, name="create_project"),
]
